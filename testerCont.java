import java.io.*;
import java.util.*;

public class testerCont
{
    public static void main(String []args) throws Exception
    {
        Scanner input = new Scanner(new FileReader("res/unique_tracks.txt"));
        Scanner sc = new Scanner(System.in);
        String data;
        ContadorEstocastico love =  new ContadorEstocastico(0.1); 
        ContadorEstocastico you = new ContadorEstocastico(0.25);  
        ContadorEstocastico god = new ContadorEstocastico(0.5);
        int godi=0, lovei=0, youi=0, wordi=0;

        System.out.print("Word: ");
        String word = sc.next();
        ContadorEstocastico wordc = new ContadorEstocastico();


        while (input.hasNext())
        {
            data = input.nextLine();
            String[] parts = data.split("<SEP>");

            if(parts.length==4)
            {

                String[] song = parts[3].split(" ");

                for (int i = 0; i < song.length; i++ )
                {
                    if (song[i].toLowerCase().equals("love"))
                    {
                        lovei+=1;
                        love.add();
                    }

                    if (song[i].toLowerCase().equals("you"))
                    {
                        youi+=1;
                        you.add(); 
                    }
                
                    if (song[i].toLowerCase().equals("god"))
                    {
                        godi+=1;
                        god.add(); 
                    } 

                    if (song[i].toLowerCase().equals(word))
                    {
                        wordi+=1;
                        wordc.add(); 
                    }
                }
            }           
        }

        System.out.println("Word    Count_Est     Real_Count");
        System.out.println("--------------------------------");
        System.out.println("love      " + love.getcount() + "        " + lovei);
        System.out.println("you       " + you.getcount() + "        " +  youi);
        System.out.println("god       " + god.getcount() + "         "  +  godi);  
        System.out.printf("%-9s %-12d %-12d\n",word,wordc.getcount(),wordi);

        input.close();
        sc.close();

    }
}