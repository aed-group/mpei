import java.util.*;

class Bloom_Filter
{
    private int size;
    private double factor;
    private short[] data;
    private int k;
    private int[] a;
    private int[] b;

    public Bloom_Filter(int size)
    {
        this.factor = 0.75;
        this.size = (int) Math.round(size / this.factor);
        this.data = new short[this.size];
        this.k = (int) Math.round((this.size / size) * Math.log(2));
        this.a = new int[this.k];
        this.b = new int[this.k];
        randHash(this.k);
    }

    public Bloom_Filter(int size, int k)
    {
        this.factor = 0.75;
        this.size = (int) Math.round(size / this.factor);
        this.data = new short[this.size];
        this.k = k;
        this.a = new int[this.k];
        this.b = new int[this.k];
        randHash(this.k);
        // initialize_data();
    }
    
    public Bloom_Filter(int size, int k, double factor)
    {
        this.factor = factor;
        this.k = k;
        this.size = (int) Math.round(size / this.factor);
        System.out.println("Bloom filter size: " + this.size);
        System.out.println("Bloom filter k: " + this.k);
        this.data = new short[this.size];
        this.a = new int[this.k];
        this.b = new int[this.k];
        randHash(this.k);
        // initialize_data();
    }

    public void randHash(int k)
    {
        Random rand = new Random();

        for (int i=0; i<k; i++)
        {
            this.a[i] = rand.nextInt((400 - 0) + 1) + 0;
            this.b[i] = rand.nextInt((750000 - 0) + 1) + 0;
        }
    }

    // private void initialize_data()
    // {
    //     for(int i=0; i<this.size; i++){
    //         if(this.data[i]!=0) System.out.println("WRONG");
    //     }
    // }

    public void insert(Object o)
    {
        for(int i=0; i<this.k; i++)
        {
            int ind = (o.hashCode()*a[i] + b[i]) % this.size;
            this.data[ind]++;
            //if(this.data[ind] < 0) System.out.println(this.data[ind]);
        }
    }

    public void delete(Object o)
    {
        if ( count(o) != 0)                     // We already check if exists in Hash_List
        {              
            for(int i=0; i<k; i++)
            {
            int ind = (o.hashCode()*a[i] + b[i]) % this.size;
                this.data[ind]--;
            }
        }
        else
            System.out.println("Wasn't there");
       
    }

    public int count(Object o)
    {
        int minor = 0;
        for(int i=0; i<k; i++)
        {
            int ind = (o.hashCode()*a[i] + b[i]) % this.size;
            if (i == 0) minor = this.data[ind];
            if ( this.data[ind] < minor) minor = this.data[ind];
        }
        return minor;
    }
}