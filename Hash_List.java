import java.util.*;

public class Hash_List<T>
{
    private int max_size;
    private LinkedList<T>[] data;
    private Bloom_Filter bf;
    // private int n_elements;

    @SuppressWarnings("unchecked")
    public Hash_List(int max_size)
    {
        this.max_size=max_size;
        this.data = new LinkedList[max_size];
        this.bf = new Bloom_Filter(max_size);
        // this.n_elements = 0;
    }

    public void insert_element(T e)
    {   
        int ind = e.hashCode()%this.max_size;
        System.out.println(ind);
        if(this.data[ind]==null)
            this.data[ind] = new LinkedList<T>();

        this.data[ind].add(e);
        this.bf.insert(e);
        
        // if(this.n_elements<max_size)
        // {
        //     this.data[n_elements] = e;
        //     this.n_elements++;
        // }
    }

    public void remove_element(T e)
    {
        int ind = e.hashCode()%this.max_size;
        if ( search(e) )
        {
            this.data[ind].remove(e);
            this.bf.delete(e);  
        }
        
        // for(int i=find_element(e); i<n_elements-1; i++){
        //     this.data[i]=data[i+1];
        // }
    }

    // private int find_element(int e)
    // {
    //     for(int i=0; i<n_elements; i++)
    //     {
    //         if(this.data[i] == e) return i;
    //     }
    // }

    public boolean search(T e)
    {
        int ind = e.hashCode()%this.max_size;
        if(this.data[ind] != null)
            return this.data[ind].contains(e);
        else
            return false;
    }

    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("Hash_List(" + max_size + "): ");
        sb.append("\n");
        for(int i=0; i<max_size; i++)
        {
            sb.append(data[i]);
            sb.append("\n");
        }
        return sb.toString();
    }
}