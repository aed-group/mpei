import java.util.*;
import java.io.*;

public class Test_MinHash{
    
    public static int search(ArrayList<Integer> usersId, int id){
        for (int i = 0; i<usersId.size();i++)
            if(usersId.get(i) == id)
                return i;
        return 0;
    }



    public static void main(String[] args) throws Exception{

        Scanner input = new Scanner(new FileReader("u.data"));
        ArrayList<User2> users = new ArrayList<>();
        ArrayList<Integer> usersId = new ArrayList<>();

        ArrayList<Movie> movies = new ArrayList<>();
        ArrayList<Integer> moviesId = new ArrayList<>();


        MinHash min_hash = new MinHash(1682,400, 943); 


        String data;
        while (input.hasNext())
        {
            data = input.nextLine();
            String[] parts = data.split("\\t");

            User2 u = new User2(Integer.parseInt(parts[0]));

            if (!usersId.contains(Integer.parseInt(parts[0]))){
                users.add(u);
                usersId.add(Integer.parseInt(parts[0]));
            }
            
            Movie mv = new Movie(Integer.parseInt(parts[1]));
            
            int ind = search(usersId,Integer.parseInt(parts[0]));
            users.get(ind).add_movie(mv);

            if (!moviesId.contains(Integer.parseInt(parts[1]))){
                movies.add(mv);
                moviesId.add(Integer.parseInt(parts[1]));
                min_hash.calc_hash_movie(mv); 
            }
        }

        input.close();

        min_hash.signaturesMovies(users);

        ArrayList<Pair> similar_users = min_hash.SimilarMovies(users, 0.4);

        for(int u=0; u<similar_users.size(); u++) {
            System.out.println(similar_users.get(u).user1 + " & " + similar_users.get(u).user2);
        }




    
    }
}