public class Pair {
    public User user1;
    public User user2;
    public int n_similar;

    public Pair(User u1, User u2, int n_similar) {
        this.user1 = u1;
        this.user2 = u2;
        this.n_similar = n_similar;
    }
}