import java.util.*;

class ContadorEstocastico
{
    private double p;
    private int count=0;
    private boolean und;

    public ContadorEstocastico(double p)
    {
        this.p = p;
        this.und = false;
    }

    public ContadorEstocastico()
    {
        this.und = true;
    }

    public void add()
    {
        double chance = Math.random();

        if (this.und == true)
            this.p = Math.pow(2,-this.count);

        if (chance <= this.p) 
        {
            // System.out.println(this.p);
            this.count++;
        }
        
    }

    public int getcount()
    {
        if (this.und == true)
            return (int)Math.pow(2,this.count)-1;
        return (int)Math.round(this.count / this.p);
    }
}
