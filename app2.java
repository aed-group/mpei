import java.util.*;
import java.io.*;

public class app2 {

    public static void main(String[] args) throws Exception{
        
        //Generate 10000 users
        int n_users = 15000;
        ArrayList<User> users = new ArrayList<>();
        ArrayList<Song> songs2 = new ArrayList<>();
        ArrayList<Song> songs = new ArrayList<>();


        MinHash min_hash_data = new MinHash(5000,100, n_users); //min-hash with 1000000 elements and 100 hash functions

        // ArrayList<Song> songs = new ArrayList<Song>();

        // Bloom_Filter bloom_filter = new Bloom_Filter(1000000); //Bloom filter for 1000000 elements

        Scanner input = new Scanner(new FileReader("res/5000tracks.txt"));
        String data;
        Song song;

        int a=0;
        while (input.hasNext())
        {
            data = input.nextLine();
            String[] parts = data.split("<SEP>");

            if(parts.length==4) {
                song = new Song(parts[3], parts[2]);

                if ( a <20 ){   //20 songs to force 2 users
                    songs2.add(song);
                    a++;
                }
                songs.add(song);
                min_hash_data.calc_hash_e(song);
                
            }

            song = null;
        }


        input.close();

        User u1 = new User();
        u1.add_song(songs2.get(0));
        u1.add_song(songs2.get(1));
        u1.add_song(songs2.get(2));
        u1.add_song(songs2.get(3));
        u1.add_song(songs2.get(8));
        u1.add_song(songs2.get(7));


        User u2 = new User();
        u2.add_song(songs2.get(0));
        u2.add_song(songs2.get(1));
        u2.add_song(songs2.get(2));
        u2.add_song(songs2.get(3));
        u2.add_song(songs2.get(4));

        users.add(u2);
        users.add(u1);

        int curr[] = new int[10];
        for (int i = 2; i < n_users; i++){

            //Select 3-10 musics to belong to this user
                int n_mus_t = (int)Math.floor(Math.random()*7 + 3); //generate number of musics
                users.add(new User());

                for(int j=0; j<n_mus_t; j++) {
                    //select random music
                    int mus_t = (int)Math.floor(Math.random()*(songs.size()-1)); //select random song
                    if (!Arrays.asList(curr).contains(mus_t)) 
                    {
                        users.get(i).add_song(songs.get(mus_t));
                        curr[j] = mus_t;
                    }
                    else
                        j--;
                }
        }

        // MinHash min_hash_data = new MinHash(songs.size(), (byte)100, users.size()); //min-hash with 1000000 elements and 100 hash functions
        // min_hash_data.calc_hash(songs);
        // songs.clear();
        min_hash_data.signatures(users);
        ArrayList<Pair> similar_users = min_hash_data.Similar(users, 0.4);

        for(int u=0; u<similar_users.size(); u++) {
            System.out.println(similar_users.get(u).user1 + " & " + similar_users.get(u).user2);
        }
        

        //System.out.println(menu(users));

        // Runtime runtime = Runtime.getRuntime(); 
        // System.out.println("max memory: " + runtime.maxMemory() / 1024); 
        // runtime = Runtime.getRuntime(); 
        // System.out.println("allocated memory: " + runtime.totalMemory() / 1024); 
        // runtime = Runtime.getRuntime(); 
        // System.out.println("free memory: " + runtime.freeMemory() / 1024); 
    }

    private static int menu(ArrayList<User> users) {
        Scanner reader = new Scanner(System.in);

        System.out.println("Users: \n");        
        for(int i=0; i<10; i++) {
            int t_usr = (int)Math.floor(Math.random()*10000);
            System.out.println(users.get(t_usr));
        }

        System.out.print("Select User: ");
        int usr = reader.nextInt();

        reader.close();
        
        return usr;
    }
    
}