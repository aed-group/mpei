import java.util.*;

class User extends User2{
    private static int idC = 0;

    public User() {
	super.id = idC;
        idC++;
	super.movies = new ArrayList<Song>();
    }

    public void add_song(Song s) {
	    super.movies.add(s);
    }

    public ArrayList<Song> getSongs() {
	    return super.getMovies();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("User: " + this.id + "\n");
        sb.append("-> Songs: \n");
        for(int i=0; i<this.getMovies().size(); i++) {
            sb.append("  " + this.getMovies().get(i) + "\n");
        }
        return sb.toString();
    }
}
