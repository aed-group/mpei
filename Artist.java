class Artist
{
    private String name;    

    public Artist(String name) 
    {
        this.name = name;
    }

    @Override
    public int hashCode()
    {
        int hash = 7; //seed
        for (int i = 0; i < this.name.length(); i++) {
            hash = (hash*31 + this.name.charAt(i)) % 5241959;
        }
        return hash;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Artist)) {
            return false;
        }
        Artist artist = (Artist) o;
        return this.name.equals(artist.name);
    }

    public String getName() {
        return this.name;
    }

    @Override
    public String toString() {
        return getName();
    }
}
