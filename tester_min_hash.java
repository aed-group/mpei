import java.util.*;
import java.io.*;

class tester_min_hash {
    public static void main(String[] args) throws Exception{
        
        Scanner input = new Scanner(new FileReader("res/unique_tracks.txt"));
        String data;

        ArrayList<Song> songs = new ArrayList<Song>();
        ArrayList<User> users = new ArrayList<User>();

        //Generate 10000 users
        for(int i=0; i<100; i++) {
            users.add(new User());
        }

        int n_elements = 0;

        while (input.hasNext())
        {
            data = input.nextLine();
            String[] parts = data.split("<SEP>");
            Song song;

            if(parts.length==4) {
                song = new Song(parts[3], parts[2]);
                songs.add(song);
                n_elements++;

                //Select 8-15 users to have this song
                int n_user_t = (int)Math.floor(Math.random()*7 + 8); //generate number of users
                //User[] users_song = new User[n_user_t];
                for(int i=0; i<n_user_t; i++) {
                    //select random user
                    int user_t = (int)Math.floor(Math.random()*100); //select random user
                    if(users.get(user_t).getSongs().size() > 10) continue;
                    users.get(user_t).add_song(song);
                }
            }
        }

        input.close();

        MinHash min_hash_data = new MinHash(n_elements, 100); //min-hash with 1000000 elements and 100 hash functions
        min_hash_data.calc_hash(songs);

        //Already have hashed list of songs
        //Signature list of users, 
        //TODO: compare, check hash code generation on SONG

        min_hash_data.signatures(users);

        for(int i=0; i<users.size(); i++) {
            System.out.println(users.get(i).getHashCodes());
        }
    }

    public void generate_users() {

    }
}