import java.util.*;

class User2 {
    public int id;
    public ArrayList<Movie> movies;
    
    public User2() {
	    this.id = 0;
	    this.movies = new ArrayList<Movie>();
    }

    public User2(int id) {
        this.id = id;
        this.movies = new ArrayList<Movie>();
    }

    public void add_movie(Movie movie) {
        this.movies.add(movie);
    }

    public ArrayList<Movie> getMovies() {
        return this.movies;
    }

    public int getId() {
        return this.id;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("User: " + this.id + "\n");
        
        return sb.toString();
    }
}
