import java.io.*;
import java.util.*;

public class tester_bloom_filter
{
    public static void main(String []args) throws Exception
    {
        Scanner input = new Scanner(new FileReader("res/10000tracks.txt"));
        Scanner sc = new Scanner(System.in);

        String data;
        Artist artist;

        Bloom_Filter counting_bloom_filter = new Bloom_Filter(45000, 7, 0.1); //Create counting bloom filter for 45000 elements whith 7 hash functions

        HashMap<Artist, Integer> rand_artists = new HashMap<>();
        byte n_artist_to_test = 100;
        byte n_artist_tested = 0;

        while (input.hasNext())
        {
            data = input.nextLine();
            String[] parts = data.split("<SEP>");

            if(parts.length==4)
                artist = new Artist(parts[2]);
            else
                artist = new Artist("No name");
                
            counting_bloom_filter.insert(artist);               //put all artists in Counting Bloom Filter            
            
            if(rand_artists.containsKey(artist)){               //put 100 artists in Map to compare later with Counting Filter
                int temp = rand_artists.get(artist);
                rand_artists.put(artist, temp+1);
            } //count number of songs from artist in rand_artists list
            else {
                if(n_artist_tested < n_artist_to_test) {
                    if(Math.random() < 0.25) {
                        rand_artists.put(artist, 1);
                        n_artist_tested++;
                    }
                }
            }
        }

        String strOut;

        System.out.println("Artist                                         Real Count    Bloom Count ");
        System.out.println("-------------------------------------------------------------------------");

        Iterator<Map.Entry<Artist, Integer>> it = rand_artists.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<Artist, Integer> pair = it.next();
            if (pair.getKey().getName().length() > 50)
                strOut = pair.getKey().getName().substring(0,47) + "...";
            else
                strOut = pair.getKey().getName();

            System.out.printf("%-53s %-10d %-10d\n",strOut,pair.getValue(),counting_bloom_filter.count(pair.getKey()));
            
            it.remove(); // avoids a ConcurrentModificationException
        }

        System.out.println("");
        System.out.println("TESTE 2 - False Positives");      //-----TESTE 2 - Check if 10000 Random Strings are in bloom given that all of them are false positives
        System.out.print("Print all false postives? (y/n): ");
        String prnt = sc.next();

        int len,randint;

        Random rand = new Random();
        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        int fls=0;
        
        for (int j=0; j<10000; j++){
            len = rand.nextInt((15 - 5) + 1) + 5; //len between 5-15
            StringBuilder buffer = new StringBuilder(len);
            for (int i = 0; i < len; i++) {
                randint = rand.nextInt((rightLimit - leftLimit) + 1) + leftLimit; //97<int<122
                buffer.append((char) randint);
            }
            String str = buffer.toString();
            artist = new Artist(str);       //Strings are Artists -> use our HashFunction

            if (counting_bloom_filter.count(artist) != 0)
            {
                if (prnt.equals("y"))
                    System.out.println("False Positive: " + str);
                fls++;
            }
        }
        System.out.println("Number os false positives (in 10000 str): " + fls);

    
        input.close();
    }
}