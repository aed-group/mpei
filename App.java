import java.util.*;
import java.util.Map.Entry;
import java.io.*;

class App {

    public static void main(String[] args) throws Exception {

        Scanner reader = new Scanner(System.in);
        
        //Generate 5000 users
        int n_users = 5000;
        ArrayList<User> users = new ArrayList<>();
        for(int i=0; i<n_users; i++) {
            users.add(new User());
        }

        MinHash min_hash_data = new MinHash(500, (byte)100, n_users); //min-hash with 1000000 elements and 100 hash functions

        ArrayList<Song> songs = new ArrayList<Song>();

        Bloom_Filter artists_bf = new Bloom_Filter(500); //Bloom filter for 500 elements

        ArrayList<ContadorEstocastico> song_counter = new ArrayList<ContadorEstocastico>();

        Scanner input = new Scanner(new FileReader("res/unique_tracks.txt"));
        String data;
        Song song;
        Artist artist;
        int n_songs = 0;

        System.out.println("Opening file...");
        
        while (input.hasNext())
        {
            if (n_songs>=500) break;

            data = input.nextLine();
            String[] parts = data.split("<SEP>");

            if(parts.length==4) {
                song = new Song(parts[3], parts[2]);
                artist = new Artist(parts[2]);

                artists_bf.insert(artist);
                songs.add(song);
                song_counter.add(new ContadorEstocastico(0.5));

                min_hash_data.calc_hash_e(song);
                
                n_songs++;
                
                //Select 50 users to have this song
                // int n_user_t = 50;//(int)Math.floor(Math.random()*(100-50) + 50); //generate number of users
                
                // for(int i=0; i<n_user_t; i++) {
                //     //select random user
                //     int user_t = (int)Math.floor(Math.random()*n_users); //select random user
                //     if(users.get(user_t).getSongs().size() > 100) {
                //         i--;
                //         continue;
                //     }
                //     users.get(user_t).add_song(song);
                // }
            }

            song = null;
            artist = null;
        }

        input.close();
        System.out.println("Finished reading file. " + songs.size() + " songs.");

        // for(int i=0; i<10; i++) {
        //     users.get(0).add_song(songs.get(i));
        //     users.get(1).add_song(songs.get(i));

        //     if (i>8) continue;
        //     users.get(2).add_song(songs.get(i));
        //     users.get(3).add_song(songs.get(i));

        //     if (i>6) continue;
        //     users.get(4).add_song(songs.get(i));
        //     users.get(5).add_song(songs.get(i));

        //     if (i>4) continue;
        //     users.get(6).add_song(songs.get(i));
        //     users.get(7).add_song(songs.get(i));
        // }

        for(int u=0; u<users.size(); u++) {
            for(int i=0; i<200; i++) {
                    //select random song
                    int songi = (int)Math.floor(Math.random()*(songs.size()-1)); //select random song
                    users.get(u).add_song(songs.get(songi));
                    song_counter.get(songi).add();
                }    
        }

        // songs.clear();

        // MinHash min_hash_data = new MinHash(songs.size(), (byte)100, users.size()); //min-hash with 1000000 elements and 100 hash functions
        // min_hash_data.calc_hash(songs);
        // songs.clear();
        System.out.println("Calculating signatures...");
        min_hash_data.signatures(users);
        System.out.println("Finished calculating signatures.");
        
        System.out.println("Calculating similar users...");        
        ArrayList<Pair> similar_users = min_hash_data.Similar(users, 0.275);
        System.out.println("Finished calculating similar users: " +  similar_users.size());        

        ArrayList<User> t_users = new ArrayList<User>();
        for(int u=0; u<similar_users.size(); u++) {
            // if(similar_users.get(u).n_similar == 100) continue;
            // System.out.println(similar_users.get(u).user1.getId() + " & " + similar_users.get(u).user2.getId() + " -> " + similar_users.get(u).n_similar);
            if(!t_users.contains(similar_users.get(u).user1))
                t_users.add(similar_users.get(u).user1);
        }   


        System.out.println("--------------------");

        User cur_usr = menu(t_users, reader);
        t_users.clear();

        System.out.println("--------------------");

        ArrayList<User> bros = new ArrayList<User>();
        for(int u=0; u<similar_users.size(); u++) {
            if (cur_usr.getId() == similar_users.get(u).user1.getId()) {
                System.out.println("This is your bro: " + similar_users.get(u).user2.getId());
                if(!bros.contains(similar_users.get(u).user2))
                    bros.add(similar_users.get(u).user2);
            }
            else if (cur_usr.getId() == similar_users.get(u).user2.getId()) {
                System.out.println("This is your bro: " + similar_users.get(u).user1.getId());
                if(!bros.contains(similar_users.get(u).user2))
                    bros.add(similar_users.get(u).user1);
            }
        }

        Bloom_Filter bloom_filter = new Bloom_Filter(cur_usr.getSongs().size()); //Bloom filter for cur_user
        ArrayList<Song> songs_to_suggest = new ArrayList<Song>(); 
        
        for(int s=0; s<cur_usr.getSongs().size(); s++) { //add songs from cur_usr into bloom_filter
            bloom_filter.insert(cur_usr.getSongs().get(s));
        }

        for(int b=0; b<bros.size(); b++) { //for each bro
            for(int s=0; s<bros.get(b).getSongs().size(); s++) { //for each song from bro
                if(bloom_filter.count(bros.get(b).getSongs().get(s))==0) { //song doesn't exist in cur_usr
                    if (!songs_to_suggest.contains(bros.get(b).getSongs().get(s)))
                        songs_to_suggest.add(bros.get(b).getSongs().get(s));
                    //System.out.println(bros.get(b).getSongs().get(s));
                }
            }
        }

        show_suggestions(songs_to_suggest, reader);

        System.out.println("--------------------");

        print_artist_occurs(cur_usr.getSongs(), artists_bf, reader);

        System.out.println("--------------------");

        get_top_songs(10, song_counter, songs, reader);
        

        // System.out.println(menu(users));

        // Runtime runtime = Runtime.getRuntime(); 
        // System.out.println("max memory: " + runtime.maxMemory() / 1024); 
        // runtime = Runtime.getRuntime(); 
        // System.out.println("allocated memory: " + runtime.totalMemory() / 1024); 
        // runtime = Runtime.getRuntime(); 
        // System.out.println("free memory: " + runtime.freeMemory() / 1024); 

        songs.clear();

        reader.close();
    }

    private static User menu(ArrayList<User> users, Scanner reader) {

        System.out.println("Users:");        
        for(int i=0; i<users.size(); i++) {
            System.out.println("User: " + users.get(i).getId());
        }

        System.out.print("Select User Id: ");
        int usr = reader.nextInt();

        for(int i=0; i<users.size(); i++) {
            if(users.get(i).getId() == usr) { 
                return users.get(i);
            }
        }
        
        return users.get(0);
    }

    private static void show_suggestions(ArrayList<Song> songs, Scanner reader) {
        int n_songs_s;

        System.out.print("How much suggestions do you want to see (0 to quit) (" + songs.size() + " available)? ");
        n_songs_s = reader.nextInt();

        int cur_s=0;
        int s;
        while (n_songs_s != 0) {
            for(s=cur_s; s<cur_s+n_songs_s && s<songs.size(); s++) {
                System.out.println(songs.get(s));
            }
            cur_s += n_songs_s;
            if(cur_s >= songs.size()) break;
            // System.out.println("(Printed " + cur_s + ")");
            System.out.print("How much suggestions do you want to see (0 to quit) (" + (songs.size() - cur_s) + " available)? ");
            n_songs_s = reader.nextInt();
        }
    }

    private static void print_artist_occurs(ArrayList<Song> songs, Bloom_Filter artists, Scanner reader) {
        System.out.print("Get count of artists you like (insert number or 0 to quit) (" + songs.size() + " available) ? ");
        
        // if(reader.nextInt() != 0)
        //     for(int s=0; s<songs.size(); s++) {
        //         System.out.println(songs.get(s).getArtist() + ": " + artists.count(new Artist(songs.get(s).getArtist())));
        //     }

        int n_songs_s = reader.nextInt();

        int cur_s=0;
        int s;
        while (n_songs_s != 0) {
            for(s=cur_s; s<cur_s+n_songs_s && s<songs.size(); s++) {
                System.out.println(songs.get(s).getArtist() + ": " + artists.count(new Artist(songs.get(s).getArtist())));
            }
            cur_s += n_songs_s;
            if(cur_s >= songs.size()) break;
            System.out.print("Get count of artists you like (insert number or 0 to quit) (" + songs.size() + " available) ? ");
            n_songs_s = reader.nextInt();
        }
    }
    
    private static void get_top_songs(int n, ArrayList<ContadorEstocastico> song_counter, ArrayList<Song> songs, Scanner reader) {
        HashMap<Song, Integer> top_songs = new HashMap<Song, Integer>();
        int i;
        for(i=0; i<n; i++)
            top_songs.put(new Song("", ""), 0); // empty counter
        for(int s=0; s<song_counter.size(); s++) {
            if(song_counter.get(s).getcount() > Collections.min(top_songs.values())) { // new max
                top_songs.put(songs.get(s), song_counter.get(s).getcount());
                // remove previous min value of top songs
                for (Entry<Song, Integer> entry : top_songs.entrySet()) { // Itrate through hashmap
                    if (entry.getValue()==Collections.min(top_songs.values())) {
                        top_songs.remove(entry.getKey()); // remove min song
                        break;
                    }
                }
            }
        }
        System.out.print("These are the top " + n + " songs (1/0): ");
        if(reader.nextInt() != 0)
            for (Map.Entry<Song, Integer> entry : top_songs.entrySet()) {  // Itrate through hashmap
                System.out.println(entry.getKey() + ": " + entry.getValue());
            }
    }
}