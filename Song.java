class Song extends Movie
{
    private static int idC = 1;
    private String name;
    private String artist;

    public Song(String name, String artist)
    {
        super(idC);

        this.name = name;
        idC++;
        this.artist=artist;
    }

    @Override
    public int hashCode()
    {
        int hash = 7; //seed
        for (int i = 0; i < this.name.length(); i++) {
            hash = (hash*31 + this.name.charAt(i)) % 65713; // 319993;
            hash = (hash*31 + this.name.charAt(i)) % 65713;
        }
        return hash;
    }

    /* public int getId() {
        return this.id;
    } */

    public String getName(){
        return this.name;
    }

    public String getArtist() {
        return this.artist;
    }

    public String toString(){
        return this.artist + " - " + this.name;
    }
}
