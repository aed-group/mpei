CC = javac
CFLAGS = -Xlint

App:
	$(CC) $(CFLAGS) App.java

all:
	$(CC) $(CFLAGS) Bloom_Filter.java
	$(CC) $(CFLAGS) Song.java
	$(CC) $(CFLAGS) User.java
	$(CC) $(CFLAGS) MinHash.java
	$(CC) $(CFLAGS) Artist.java
	$(CC) $(CFLAGS) ContadorEstocastico.java
	$(CC) $(CFLAGS) tester_min_hash.java
	$(CC) $(CFLAGS) testerCont.java
	$(CC) $(CFLAGS) tester_bloom_filter.java
	$(CC) $(CFLAGS) App.java

Hash_List:
	$(CC) $(CFLAGS) Hash_List.java

tester:
	$(CC) $(CFLAGS) Hash_List.java
	$(CC) $(CFLAGS) tester.java
	$(CC) $(CFLAGS) Bloom_Filter.java
	$(CC) $(CFLAGS) Genre.java
	$(CC) $(CFLAGS) Song.java
	$(CC) $(CFLAGS) Artist.java

tester_bloom_filter:
	$(CC) $(CFLAGS) Bloom_Filter.java
	$(CC) $(CFLAGS) Artist.java
	$(CC) $(CFLAGS) tester_bloom_filter.java

testerCont:
	$(CC) $(CFLAGS) ContadorEstocastico.java
	$(CC) $(CFLAGS) testerCont.java

tester_min_hash:
	$(CC) $(CFLAGS) Song.java
	$(CC) $(CFLAGS) User.java
	$(CC) $(CFLAGS) MinHash.java
	$(CC) $(CFLAGS) tester_min_hash.java