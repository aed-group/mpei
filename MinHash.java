import java.util.*;

class MinHash {
    private int size;
    private int K;
    private int n_users;
    private int[][] hash_data;
    private int[][] signatures;
    private int[] a;
    private int[] b;

    
    public MinHash(int size, int K, int n_users) {
        this.size = size;
        this.K = K;
        this.n_users = n_users;
        this.hash_data = new int[this.size][this.K];
        this.signatures = new int[this.n_users][this.K];
        this.a = new int[this.K];
        this.b = new int[this.K];
        randHash(this.K);

    }

    public void randHash(int k)
    {
        Random rand = new Random();

        for (int i=0; i<k; i++)
        {
            this.a[i] = rand.nextInt((6500 - 0) + 1) + 0;
            this.b[i] = rand.nextInt((5000000 - 0) + 1) + 0;
            this.a[i] = rand.nextInt(28000);
            this.b[i] = rand.nextInt(5000000);
        }
    }

    public void calc_hash_e(Song song) {
        for(int k=0; k<this.K; k++) {
            this.hash_data[song.getId()][k] = (song.hashCode()*a[k] + b[k]) % this.size;
        }
    }

    public void calc_hash_movie(Movie movie) {
        for(int k=0; k<this.K; k++) {
            this.hash_data[movie.getId()-1][k] = (movie.hashCode()*a[k] + b[k]) % this.size;
        }
    }

    public void signatures(ArrayList<User> users) {
        for(int u=0; u<this.n_users; u++) { //for each user
            int s_size = users.get(u).getSongs().size(); // n of songs from user
            if (s_size == 0) {
                for(int k=0; k<this.K; k++) {
                    this.signatures[u][k] = 0;
                }
                continue; 
            }
            int[] k_songs = new int[s_size]; //hash codes from songs at current k
            for(int k=0; k<this.K; k++) { //for each k
                for(int s=0; s<s_size; s++) {
                    k_songs[s] = this.hash_data[users.get(u).getSongs().get(s).getId()][k];
                }
                this.signatures[u][k] = min(k_songs);
            }
        }
        this.hash_data = null;
    } 

    public void signaturesMovies(ArrayList<User2> users) {
        for(int u=0; u<this.n_users; u++) { //for each user
            int s_size = users.get(u).getMovies().size(); // n of songs from user
            int[] k_movies = new int[s_size]; //hash codes from songs at current k
            for(int k=0; k<this.K; k++) { //for each k
                for(int s=0; s<s_size; s++) {
                    k_movies[s] = this.hash_data[users.get(u).getMovies().get(s).getId()-1][k];
                }
                this.signatures[u][k] = min(k_movies);
            }
        }
        this.hash_data = null;
    }


    // public void getSignatures(int u) {
    //     for(int i=0; i<this.K; i++)
    //         System.println(this.signatures[u][i]);
    // }

    private int min(int[] n) {
        int min = n[0];
        for(int i = 1; i<n.length; i++) {
            if(n[i] < min) min = n[i];
        }
        return min;
    }

    public ArrayList<Pair> Similar(ArrayList<User> users, double threshold){
        ArrayList<Pair> similar_users = new ArrayList<>();
        double min = 1.0;
        int count;
        double jac=0;
        for (int u1 = 0; u1 < this.n_users; u1++) {
            for (int u2 = u1+1; u2 < this.n_users; u2++) {
                count = 0; 
                for (int k = 0; k<this.K; k++){
                    if ( this.signatures[u1][k] == this.signatures[u2][k]){
                        count++;    
                    }                   
                }
                jac = 1 - ((double)count)/this.K;
                if (jac < min) min = jac;
                // System.out.println("User 1: " + u1 + "; User 2: " + u2 + " --> " + jac);

                if(jac < threshold) 
                    similar_users.add(new Pair(users.get(u1), users.get(u2), count));                    
            }
        }
        return similar_users;
    }

    public ArrayList<Pair> SimilarMovies(ArrayList<User2> users, double threshold){
        ArrayList<Pair> similar_users = new ArrayList<>();
        int count;
        double jac=0;
        for (int u1 = 0; u1 < this.n_users; u1++) {
            for (int u2 = u1+1; u2 < this.n_users; u2++) {
                count = 0; 
                for (int k = 0; k<this.K; k++){
                    if ( this.signatures[u1][k] == this.signatures[u2][k]){
                        count++;    
                    }                   
                }
                jac = 1 - ((double)count)/this.K;

                if(jac < threshold) {
                    similar_users.add(new Pair(users.get(u1), users.get(u2), count));
                    //System.out.println("A: " + similar_users.size());
                    System.out.println(jac + " count: " + count);

                    similar_users.add(new Pair(users.get(u1), users.get(u2), count));
                    // System.out.println("A: " + similar_users.size());
                }
            }
        }
        System.out.println("Min jac: " + min);
        return similar_users;
    }
}
